#!/usr/bin/env python3

import os
from fnmatch import fnmatch
from datetime import datetime

from flask import Flask, jsonify, url_for

app = Flask(__name__)
app.config["PROPAGATE_EXEPTIONC"] = True

ZIP_PATH = os.environ["LINEAGE_OTA_ZIPS"]

def get_hash(file_name):
    hash_file = os.path.join(ZIP_PATH, 
            file_name + ".sha256sum")
    if not os.path.exists(hash_file):
        raise Exception("Hash file {} for zip {} not provided.".format(
            hash_file, file_name))
    with open(hash_file, "r") as f:
        line = f.read().splitlines()[0]
        if line.split()[1] == file_name:
            return line.split()[0]
        else:
            raise Exception("File name {} did not match expected file name {}"
                    .format(line.split()[1], file_name))

def get_meta_info(file_name):
    meta_info = {}
    meta_info["datetime"] = int(datetime.strptime(
        file_name.split("-")[2], "%Y%m%d").timestamp())
    meta_info["filename"] = file_name
    meta_info["id"] = get_hash(file_name)
    meta_info["romtype"] = file_name.split("-")[3]
    meta_info["size"] = os.path.getsize(os.path.join(ZIP_PATH, file_name))
    meta_info["url"] = url_for("download", filename=file_name, _external=True)
    meta_info["version"] = file_name.split("-")[1]
    return meta_info

@app.route("/")
@app.route("/<api_version>/<device>/<build_type>/<incr>")
def index(api_version=None, device=None, build_type=None, incr=None):
    builds = []
    for file_name in os.listdir(ZIP_PATH):
        if fnmatch(file_name, "*.zip"):
            builds.append(get_meta_info(file_name))
    response = {}
    response["response"] = builds
    return jsonify(response)

@app.route("/download/<filename>")
def download(filename):
    raise Exception("Should never be called. Only used as a stub "
        "for flask.url_for")

if __name__ == "__main__":
    app.debug = True
    app.run()
