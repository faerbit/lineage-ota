FROM alpine:3.12

EXPOSE 80

RUN apk add --no-cache \
        dumb-init \
        nginx \
        uwsgi-python3 \
        py3-pip \
        python3 

RUN sed -i '/worker_processes/d' /etc/nginx/nginx.conf

RUN pip3 install --user --no-cache-dir pipenv

RUN mkdir -p /app/src
WORKDIR /app/src

ADD Pipfile .
ADD Pipfile.lock .
ADD server.py .

RUN PIPENV_VENV_IN_PROJECT=1 /root/.local/bin/pipenv sync

VOLUME ["/zips" ]

ENV LINEAGE_OTA_ZIPS=/zips

ADD nginx.conf /etc/nginx/conf.d/default.conf
ADD run.sh /root

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD [ "/root/run.sh" ]
