#!/bin/sh


mkdir /run/uwsgi
chown uwsgi:uwsgi /run/uwsgi
uwsgi --socket /run/uwsgi/lineage.sock \
    --chmod-socket=777 \
    --uid uwsgi \
    --plugins python3 \
    --protocol uwsgi \
    --master \
    --virtualenv .venv \
    --module server:app &

mkdir /run/nginx
nginx -g "daemon off; worker_processes 1; error_log /dev/stderr info;"
